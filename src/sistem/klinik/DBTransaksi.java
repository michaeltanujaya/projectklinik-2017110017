import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;
import sistem.klinik.Koneksi;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Multimedia
 */
public class DBTransaksi {
    int id_pasien;
    String tanggal;
    int id_poli;
    int jenis;
    float harga;	
    float lama_inap;
    int total;
    public int getId_pasien() {
        return id_pasien;
    }

    public void setId_pasien(int id_pasien) {
        this.id_pasien = id_pasien;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public int getId_poli() {
        return id_poli;
    }

    public void setId_poli(int id_poli) {
        this.id_poli = id_poli;
    }

    public int getJenis() {
        return jenis;
    }

    public void setJenis(int jenis) {
        this.jenis = jenis;
    }

    public float getHarga() {
        return harga;
    }

    public void setHarga(float harga) {
        this.harga = harga;
    }

    public float getLama_inap() {
        return lama_inap;
    }

    public void setLama_inap(float lama_inap) {
        this.lama_inap = lama_inap;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    
    
  
    
    public boolean insertMaster(){
        boolean berhasil=false;
        Koneksi con=new Koneksi();
        try {
            con.bukaKoneksi();
            con.preparedStatement = con.dbKoneksi.prepareStatement("Insert into Pasien"+
                    "(tanggal,id_pasien, total) values (now(),0,?)");
                     con.preparedStatement.executeUpdate();
                        
                     
         berhasil=true;
        } catch(Exception e){
            e.printStackTrace();
            berhasil=false;
        } finally {
          con.tutupKoneksi();  
          return berhasil;          
        }
    }
    public boolean insertDetil(){
        boolean berhasil=false;
        Koneksi con=new Koneksi();
        try {
            
            con.bukaKoneksi();
            ResultSet rs = con.statement.executeQuery("select max(idPenjualan from penjualan");
            int i=1;
            while(rs.next()){
                id_pasien= (rs.getInt("id_pasien"));
                i++;
            }
            con.preparedStatement = con.dbKoneksi.prepareStatement("Insert into penjualan"+
                  "(id_pasien,id_poli, jenis,harga,lama_inap) values (now(),0,?)");
                   con.preparedStatement.setInt(1, this.id_pasien);
                   con.preparedStatement.setInt(2, this.id_poli);
                   con.preparedStatement.setInt(3, this.jenis);
                   con.preparedStatement.setFloat(4, this.harga);
                   con.preparedStatement.setFloat(5, this.lama_inap);
                   con.preparedStatement.executeUpdate();    
        berhasil=true;
        } catch(Exception e){
            e.printStackTrace();
            berhasil=false;
        } finally {
          con.tutupKoneksi();  
          return berhasil;          
        }
    }
     public Vector LookUp(String fld, String dt){
        try{
            Vector tableData= new Vector();
            Koneksi con=new Koneksi();
            con.bukaKoneksi();
            con.statement = con.dbKoneksi.createStatement();
            ResultSet rs = con.statement.executeQuery("select id, nama_barang, harga "+
                    " from barang where "+fld+" like '%"+dt+"%'");
            
          int i=1;
          while(rs.next()){
              Vector<Object> row = new Vector<Object>();
              row.add(rs.getString("id"));
              row.add(rs.getString("nama_barang"));
              row.add(rs.getFloat("harga"));
              row.add(i);
              tableData.add(row);
              i++;
          }
          con.tutupKoneksi();
          return tableData;  
        } catch (SQLException ex){
            ex.printStackTrace();
          return null;
        }
    } 
}
