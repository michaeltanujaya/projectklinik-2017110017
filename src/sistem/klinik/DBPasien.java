/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistem.klinik;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

/**
 *
 * @author Multimedia
 */
public class DBPasien {
    int id_pasien;
    String nama;
    String alamat;
    int no_tlp;

    public int getId_pasien() {
        return id_pasien;
    }

    public void setId_pasien(int id_pasien) {
        this.id_pasien = id_pasien;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public int getNo_tlp() {
        return no_tlp;
    }

    public void setNo_tlp(int no_tlp) {
        this.no_tlp = no_tlp;
    }
    
     public boolean insert(){
        boolean berhasil=false;
        Koneksi con=new Koneksi();
        try {
            con.bukaKoneksi();
            con.preparedStatement = con.dbKoneksi.prepareStatement("Insert into pasien"+
                    "(id_pasien, nama, alamat, no_tlp) values (?,?,?,?)");

         berhasil=true;
        } catch(Exception e){
            e.printStackTrace();
            berhasil=false;
        } finally {
          con.tutupKoneksi();  
          return berhasil;          
        }
    }
     
     public boolean update(int id_pasien){
        boolean berhasil = false;
        Koneksi con = new Koneksi();
        try{
            con.bukaKoneksi();
            con.preparedStatement = con.dbKoneksi.prepareStatement("update barang set nama=?"+
                    " ,alamat=?  where id_pasien = ?");
                     con.preparedStatement.setInt(1, this.id_pasien);
                     con.preparedStatement.setString(2, this.nama);
                     con.preparedStatement.setString(3, this.alamat);
                     con.preparedStatement.setInt(4, this.no_tlp);
                     con.preparedStatement.executeUpdate();
                     berhasil = true;
                     
        } catch (Exception e){
            e.printStackTrace();
            berhasil = false;
        } finally {
            con.tutupKoneksi();
            return berhasil;
        }
    }
    
     public Vector Load(){
        try {
          Vector tableData = new Vector();
          Koneksi con=new Koneksi();
          con.bukaKoneksi();
          con.statement = con.dbKoneksi.createStatement();
          ResultSet rs = con.statement.executeQuery("id_pasien, nama, alamat, no_tlp"+
                  " from pasien");
          int i=1;
          while(rs.next()){
              Vector<Object> row = new Vector<Object>();
              row.add(rs.getInt("id_pasien"));
              row.add(rs.getString("nama"));
              row.add(rs.getString("alamat"));
              row.add(rs.getInt("no_tlp"));
              row.add(i);
              tableData.add(row);
              i++;
          }
          con.tutupKoneksi();
          return tableData;  
        } catch (SQLException ex){
            ex.printStackTrace();
          return null;
        }
    }
    
    public boolean delete(int id){
        boolean berhasil = false;
        Koneksi con = new Koneksi();
        try{
            con.bukaKoneksi();
            con.preparedStatement = con.dbKoneksi.prepareStatement("delete from barang where id_pasien"+
                    " = ? ");
            con.preparedStatement.setInt(1, id_pasien);
            con.preparedStatement.executeUpdate();
            berhasil = true;
        } catch(Exception e){
            e.printStackTrace();
            berhasil = false;
        } finally {
            con.tutupKoneksi();
            return berhasil;
        }
    }
    
    public boolean select(int id_pasien){
        try {
            Koneksi con = new Koneksi();
            con.bukaKoneksi();
            con.statement = con.dbKoneksi.createStatement();
            ResultSet rs = con.statement.executeQuery("select id_pasien, nama, alamat, no_tlp"+
                    " from barang where id_pasien = "+id_pasien);
            while (rs.next()){
                this.id_pasien = rs.getInt("id_pasien");
                this.nama = rs.getString("nama");
                this.alamat = rs.getString("alamat");
                this.no_tlp = rs.getInt("no_tlp");
            }
            con.tutupKoneksi();
            return true;
        } catch(SQLException ex){
            ex.printStackTrace();
            return false;
        }
    }
<<<<<<< HEAD
     public int validasiID(String nama, String harga){
        int val = 0;
        try{
            Koneksi con = new Koneksi();
            con.bukaKoneksi();
            con.statement = con.dbKoneksi.createStatement();
            ResultSet rs = con.statement.executeQuery("Select count(*) as jml "+
         " from barang where id = '"+nama+"'");
            while(rs.next()){
                val = rs.getInt("jml");
            }
            con.tutupKoneksi();
        } catch (SQLException ex){
            ex.printStackTrace();                    
        }

        return val;
    }
     public Vector LookUp(String fld, String dt){
        try{
            Vector tableData= new Vector();
            Koneksi con=new Koneksi();
            con.bukaKoneksi();
            con.statement = con.dbKoneksi.createStatement();
            ResultSet rs = con.statement.executeQuery("select id, nama_barang, harga "+
                    " from barang where "+fld+" like '%"+dt+"%'");
            
          int i=1;
          while(rs.next()){
              Vector<Object> row = new Vector<Object>();
              row.add(rs.getString("id"));
              row.add(rs.getString("nama_barang"));
              row.add(rs.getFloat("harga"));
              row.add(i);
              tableData.add(row);
              i++;
          }
          con.tutupKoneksi();
          return tableData;  
        } catch (SQLException ex){
            ex.printStackTrace();
          return null;
        }
    } 
=======
    
>>>>>>> master
    
}
