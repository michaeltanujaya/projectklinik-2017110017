/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistem.klinik;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

/**
 *
 * @author Multimedia
 */
public class DBPoliKlinik {
    int id_poli;
    String jenis;
    Date jampraktek;
    int lama_inap;
    String nama_ruang;
    int harga;

    public int getId_poli() {
        return id_poli;
    }

    public void setId_poli(int id_poli) {
        this.id_poli = id_poli;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public Date getJampraktek() {
        return jampraktek;
    }

    public void setJampraktek(Date jampraktek) {
        this.jampraktek = jampraktek;
    }

    public int getLama_inap() {
        return lama_inap;
    }

    public void setLama_inap(int lama_inap) {
        this.lama_inap = lama_inap;
    }

    public String getNama_ruang() {
        return nama_ruang;
    }

    public void setNama_ruang(String nama_ruang) {
        this.nama_ruang = nama_ruang;
    }

    public int getHarga() {
        return harga;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }
    
    
      public boolean insert(){
        boolean berhasil=false;
        Koneksi con=new Koneksi();
        try {
            con.bukaKoneksi();
            con.preparedStatement = con.dbKoneksi.prepareStatement("Insert into pasien"+
                    "(id_poli, jenis, jampraktek, lama_inap, nama_ruang, harga) values (?,?,?,?,?,?)");

         berhasil=true;
        } catch(Exception e){
            e.printStackTrace();
            berhasil=false;
        } finally {
          con.tutupKoneksi();  
          return berhasil;          
        }
    }
     
     public boolean update(int id_pasien){
        boolean berhasil = false;
        Koneksi con = new Koneksi();
        try{
            con.bukaKoneksi();
            con.preparedStatement = con.dbKoneksi.prepareStatement("update barang set jenis=?"+
                    " ,nama_ruang=?  where id_poli = ?");
                     con.preparedStatement.setInt(1, this.id_poli);
                     con.preparedStatement.setString(2, this.jenis);
                     con.preparedStatement.setDate(3, this.jampraktek);
                     con.preparedStatement.setInt(4, this.lama_inap);
                     con.preparedStatement.setString(2, this.nama_ruang);
                     con.preparedStatement.setInt(4, this.harga);
                     con.preparedStatement.executeUpdate();
                     berhasil = true;
                     
        } catch (Exception e){
            e.printStackTrace();
            berhasil = false;
        } finally {
            con.tutupKoneksi();
            return berhasil;
        }
    }
    
     public Vector Load(){
        try {
          Vector tableData = new Vector();
          Koneksi con=new Koneksi();
          con.bukaKoneksi();
          con.statement = con.dbKoneksi.createStatement();
          ResultSet rs = con.statement.executeQuery("id_poli, jenis, jampraktek, lama_inap, nama_ruang, harga"+
                  " from poliklinik");
          int i=1;
          while(rs.next()){
              Vector<Object> row = new Vector<Object>();
              row.add(rs.getInt("id_poli"));
              row.add(rs.getString("jenis"));
              row.add(rs.getDate("jampraktek"));
              row.add(rs.getInt("lama_inap"));
              row.add(rs.getString("nama_ruang"));
              row.add(rs.getInt("harga"));
              row.add(i);
              tableData.add(row);
              i++;
          }
          con.tutupKoneksi();
          return tableData;  
        } catch (SQLException ex){
            ex.printStackTrace();
          return null;
        }
    }
    
    public boolean delete(int id){
        boolean berhasil = false;
        Koneksi con = new Koneksi();
        try{
            con.bukaKoneksi();
            con.preparedStatement = con.dbKoneksi.prepareStatement("delete from barang where id_poli"+
                    " = ? ");
            con.preparedStatement.setInt(1, id_poli);
            con.preparedStatement.executeUpdate();
            berhasil = true;
        } catch(Exception e){
            e.printStackTrace();
            berhasil = false;
        } finally {
            con.tutupKoneksi();
            return berhasil;
        }
    }
    
    public boolean select(int id_poli){
        try {
            Koneksi con = new Koneksi();
            con.bukaKoneksi();
            con.statement = con.dbKoneksi.createStatement();
            ResultSet rs = con.statement.executeQuery("select id_poli, jenis, jampraktek, lama_inap, nama_ruang, harga"+
                    " from barang where id_poli = "+id_poli);
            while (rs.next()){
                this.id_poli = rs.getInt("id_poli");
                this.jenis = rs.getString("jenis");
                this.jampraktek = rs.getDate("jampraktek");
                this.lama_inap = rs.getInt("lama_inap");
                this.nama_ruang = rs.getString("nama_ruang");
                this.harga = rs.getInt("harga");
            }
            
            con.tutupKoneksi();
            return true;
        } catch(SQLException ex){
            ex.printStackTrace();
            return false;
        }
    }
<<<<<<< HEAD
      public int validasiID(String nama, String harga){
        int val = 0;
        try{
            Koneksi con = new Koneksi();
            con.bukaKoneksi();
            con.statement = con.dbKoneksi.createStatement();
            ResultSet rs = con.statement.executeQuery("Select count(*) as jml "+
         " from barang where id = '"+nama+"'");
            while(rs.next()){
                val = rs.getInt("jml");
            }
            con.tutupKoneksi();
        } catch (SQLException ex){
            ex.printStackTrace();                    
        }

        return val;
    }
=======
    
>>>>>>> master
}
