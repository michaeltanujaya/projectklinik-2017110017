/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistem.klinik;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Multimedia
 */
public class Koneksi {
    public Connection dbKoneksi;
    public Statement statement;
    public PreparedStatement preparedStatement;
    
    public Koneksi(){
        this.dbKoneksi = null;
    }
    public void bukaKoneksi(){
        try{
            Class.forName("com.mysql.jdbc.Driver");
            dbKoneksi = DriverManager.getConnection("jdbc:mysql://localhost:3306/dbklinik_2017110017?user=root");
         
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    public void tutupKoneksi(){
        try{
            if(statement!=null) statement.close();
            if(preparedStatement!=null) preparedStatement.close();
            if(dbKoneksi!=null) dbKoneksi.close();
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
            
        }
    }
    public int validasiID(String nama, String alamat){
        int val = 0;
        try{
            Koneksi con = new Koneksi();
            con.bukaKoneksi();
            con.statement = con.dbKoneksi.createStatement();
            ResultSet rs = con.statement.executeQuery("Select count(*) as jml "+
         " from barang where id_pasien = '"+nama+"'");
            while(rs.next()){
                val = rs.getInt("jml");
            }
            con.tutupKoneksi();
        } catch (SQLException ex){
            ex.printStackTrace();                    
        }

        return val;
    }
}
